package com.lp24.projet.view;

import javax.swing.JFrame;

import com.lp24.projet.controller.Mouse;


public class Window extends JFrame {
	
	private static final long serialVersionUID = 1L;

   
	public Window(Scene scene)
	{
		/*
		 
		 	Creation of a window called Lemming with a Scene as Default Panel and more others specifications
		 
		 */
		super.setTitle("Lemming");
		super.setSize(700, 360);
		super.setLocationRelativeTo(null);
		super.setResizable(false);
		super.setAlwaysOnTop(true);
		super.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		
		super.setContentPane(scene);
		
	    
		Mouse mouse = new Mouse();
		super.addMouseListener(mouse);
		super.setVisible(true);
	}

}
