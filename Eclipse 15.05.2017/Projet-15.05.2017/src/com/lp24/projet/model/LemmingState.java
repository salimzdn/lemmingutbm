package com.lp24.projet.model;

public enum LemmingState {

	
	/*
	 * 
	 * 
	 	This enum is all the capacities that a lemming can have,
	 	one LemmingState object is created in every lemming object and will define their capacity,
	 	so when we affect a capacity to a lemming we will change this object in the lemming
	 * 
	 * 
	 */
	Walk,
	Stop,
	Dig,
	Parachute;
}
