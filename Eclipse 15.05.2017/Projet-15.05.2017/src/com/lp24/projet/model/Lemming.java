package com.lp24.projet.model;

public class Lemming {

	public static final int HEIGHT=42;
	public static final int WIDTH =15;
	public static final int TIME_FOR_APPARITION =8000;
	
	public final int DIG_SPEED=10;
	private int x;
	private int y;
	private boolean alive;
	private boolean arrived;
	private boolean direction;//0=LEFT 1=RIGHT
	private int haauteurMortelle=100;//A changer de nom
	private int fallHeight;
	private LemmingState state;
	
	//Constructor
	public Lemming(int x,int y)
	{
		this.x=x;
		this.y=y;
		this.alive=true;
		this.arrived=false;
		this.direction=true;
		this.state= LemmingState.Walk;
	}


	
	//Getter
	
	public int getX() {
		return x;
	}


	public int getY() {
		return y;
	}


	public boolean isAlive() {
		return alive;
	}


	public boolean isArrived() {
		return arrived;
	}


	public boolean isDirection() {
		return direction;
	}

	public int getFallHeight() {
		return fallHeight;
	}


	public int getHaauteurMortelle() {
		return haauteurMortelle;
	}


	public LemmingState getState() {
		return state;
	}

	
	//Setter
	
	public void setX(int x) {
		this.x = x;
	}



	public void setY(int y) {
		this.y = y;
	}



	public void setAlive(boolean alive) {
		this.alive = alive;
	}



	public void setArrived(boolean arrived) {
		this.arrived = arrived;
	}



	public void setDirection(boolean direction) {
		this.direction = direction;
	}

	public void setFallHeight(int fallHeight) {
		this.fallHeight= fallHeight;
	}

	public void setHaauteurMortelle(int haauteurMortelle) {
		this.haauteurMortelle = haauteurMortelle;
	}



	public void setState(LemmingState state) {
		this.state = state;
	}
	
	
	//Methods
	
	public void walk()
	{
		//if a lemming go to the right his x position will be increase by 1
		if(this.direction==true)
		{
			this.x++;
		}
		else//if a lemming go to the left his x position will be decrease by 1
		{
			this.x--;
		}
		
	}
	
	public void fall()
	{
		//if a lemming fall his y position will be increase by 1, as his fallHeight
		
		this.y++;
		this.fallHeight++;
	}
	
	public void backToGround()
	{
		/*
		 * 
		 * 
		 	Test if a lemming must die when he touch the ground (if he fall from to high without a parachute) or not
		 * 
		 * 
		 */
		if(this.fallHeight>this.haauteurMortelle && this.state!=LemmingState.Parachute)
		{
			this.alive=false;
		}
		else
		{
			this.fallHeight=0;
		}
	}
	
	public void dig(Ground g)
	{
		/*
		 * 
		 	A ground g is dug (it will loose DIG_SPEED life points every loop round)
		 * 
		 */
		if(this.state==LemmingState.Dig)
		{
			g.setHealth(g.getHealth()-this.DIG_SPEED);
		}
	}
	
	public boolean contactDown(Block g)
	{
		/*
		 
		test if lemming is to much on the left, or on the right, 
		or too high or too low
		then there are not contact
		else there is
		 
		 */
		if (this.x+Lemming.WIDTH <g.getX()|| this.x >g.getX()+Ground.WIDTH|| 
				this.y+Lemming.HEIGHT<g.getY() || this.y>g.getY()+Ground.HEIGHT)
		{
			return false;
		}
		else 	
		{
			return true;
		}
	}
	
	public boolean contactRight(Block w)
	{
		/*
		 
		test if lemming is to much on the left, or on the right, 
		or too high or too low
		then there are not contact
		else there is
		 
		 */
		
		if (this.x + Lemming.WIDTH<w.getX()|| this.x >w.getX()+Wall.WIDTH|| 
				this.y+Lemming.HEIGHT<w.getY() || this.y>w.getY()+Wall.HEIGHT)
		{
			return false;
		}
		else
		{
			return true;
		}
	}
	
	public boolean contactLeft(Block w)
	{
		/*
		 
		test if lemming is to much on the left, or on the right, 
		or too high or too low
		then there are not contact
		else there is
		 
		 */
		if (this.x >w.getX()+Wall.WIDTH|| this.x +Lemming.WIDTH<w.getX()|| 
				this.y+Lemming.HEIGHT<=w.getY() || this.y>=w.getY()+Wall.HEIGHT)
			{
				return false;
			}
			else {
				return true;
			}
	}
	
	public boolean contactRight(Lemming l)
	{
		/*
		 Test of a stopContact
		test if lemming x position+ his width is equal to the x position of the other lemming
		and if they are in the same y position
		 then there is a rightContact with the Stop Lemming
		 */
		if(this.getX()+Lemming.WIDTH==l.getX() && this.getY()==l.getY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public boolean contactLeft(Lemming l)
	{
		/*
		 Test of a stopContact
		test if lemming x position is equal to the x position of the other lemming+ his width
		and if they are in the same y position
		 then there is a leftContact with the Stop Lemming
		 */
		if(this.getX()==l.getX()+Lemming.WIDTH && this.getY()==l.getY())
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
}
