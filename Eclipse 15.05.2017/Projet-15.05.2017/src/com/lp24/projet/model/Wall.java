package com.lp24.projet.model;

public class Wall extends Block{

	public static final int HEIGHT=40;
	public static final int WIDTH =10;
	
	public Wall(int x,int y)
	{
		this.x=x;
		this.y=y;
	}

	
	//Getter
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}	
	
	//Setter
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}
	
}
