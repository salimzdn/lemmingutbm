package com.lp24.projet.model;

public class Ground extends Block{

	public static final int HEIGHT=28;
	public static final int WIDTH =35;
	private int health;
	
	
	//Constructor
	
	public Ground(int x,int y)
	{
		this.x=x;
		this.y=y;
		this.setHealth(1000);
	}

	
	//Getter
	
	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}	

	public int getHealth() {
		return health;
	}

	//Setter
	
	public void setX(int x) {
		this.x = x;
	}

	public void setY(int y) {
		this.y = y;
	}

	public void setHealth(int health) {
		this.health = health;
	}
}
