package com.lp24.projet.model;

public enum GameState {
	/*
	 * 
	 * 
	 	This enum is all the position in the game where we can be,
	 	one GameState object is created in the main class and will define our position
	 * 
	 * 
	 */
	Menu,
	Game,
	Victory,
	Defeat;
}
