package com.lp24.projet.controller;

import java.awt.Graphics2D;
import java.io.FileNotFoundException;
import java.net.URISyntaxException;

import com.lp24.projet.main.Main;
import com.lp24.projet.model.GameState;
import com.lp24.projet.model.LemmingState;

public class Game {

	
	private int PAUSE=50;
	private Level level;
	private int time;
	private int repetitionNumber;
	private LemmingState mouseState=LemmingState.Stop;
	
	public Game(int level)
	{
		this.level=new Level(level);
	}
	
	public Level getLevel() {
		return level;
	}

	public LemmingState getMouseState() {
		return mouseState;
	}

	public int getPause() {
		return PAUSE;
	}

	public void setMouseState(LemmingState mouseState) {
		this.mouseState = mouseState;
	}

	public void setLevel(Level level) {
		this.level = level;
	}
	
	public void setPause(int Pause) {
		this.PAUSE = Pause;
	}



	public void run(Graphics2D g2)
	{
		//First we test if we have win or loose
		this.victoryTest();
		this.defeatTest();
		
		//Then we stop during a PAUSE time
		try {
			Thread.sleep(PAUSE);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		if(this.level.getNbLemming()<this.level.getMaxLemming() && this.apparitionTest())
		{//Appartion until there are not enough lemming and every 5 seconds
			this.level.addLemming();
		}
		
		this.repetitionNumber++;//It will help to create a timer
		
		
		//See description of these methods in method's codes
		
		this.level.outOfGame();
		
		this.level.wallContact();
		
		if(this.level.getNbStop()>0)
		{
			this.level.stop();
			this.level.stopContact();
		}
		if(this.level.getNbWalk()>0 || this.level.getNbParachute()>0)
		{
			this.level.move();
		}
		if(this.level.getNbDig()>0)
		{
			this.level.dig();
		}
		
		if(this.repetitionNumber-20*this.time>=0)
		{
			this.time++;
		}
		
		this.level.isArrived();
		this.level.print(g2);
		g2.drawString(""+this.time, 10, 10);
	}
	
	public boolean apparitionTest()
	{
		/*
		 * 
		  	return true if we are in multiple of 5 seconds
		 * 
		 */
		
		if(this.time%5==0 && this.level.getNbLemming()*5+5==this.time)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void defeatTest()
	{
		//If total lemming still alive are not enough to win so you loose
		if(this.level.getMaxLemming()-this.level.getNbLemmingDead()<this.level.getNbLemmingToWin())
		{
			Main.gamestate=GameState.Defeat;
		}
	}
	
	public void victoryTest()
	{
		//If enough lemming are arrived so you win
		if(this.level.getNbLemmingArrived()>=this.level.getNbLemmingToWin())
		{
			Main.gamestate=GameState.Victory;
		}
	}

	public boolean testNbInState()
	{
		//Necessary method for Mouse class witch test if you still can affect a capacity
		if(this.mouseState==LemmingState.Stop && this.level.getNbStop()<this.level.getMaxStop())
		{
			this.level.setNbStop(this.level.getNbStop()+1);
			return true;
		}
		else if(this.mouseState==LemmingState.Dig && this.level.getNbDig()<this.level.getMaxDig())
		{
			this.level.setNbDig(this.level.getNbDig()+1);
			return true;
		}
		else if(this.mouseState==LemmingState.Walk && this.level.getNbWalk()<this.level.getMaxWalk())
		{
			this.level.setNbWalk(this.level.getNbWalk()+1);
			return true;
		}
		else if(this.mouseState==LemmingState.Parachute && this.level.getNbParachute()<this.level.getMaxParachute())
		{
			this.level.setNbParachute(this.level.getNbParachute()+1);
			return true;
		}
		else
		{
			return false;
		}
		
	}
	
	public void unsetLemmingState(LemmingState st)
	{
		//Remove the capacity to a lemming, so we have to change the nb of lemming who have this capacity
		if(st.equals(LemmingState.Walk))
		{
			this.level.setNbWalk(this.level.getNbWalk()-1);
		}
		else if(st.equals(LemmingState.Dig))
		{
			this.level.setNbDig(this.level.getNbDig()-1);
		}
		else if(st.equals(LemmingState.Stop))
		{
			this.level.setNbStop(this.level.getNbStop()-1);
		}
		else if(st.equals(LemmingState.Parachute))
		{
			this.level.setNbParachute(this.level.getNbParachute()-1);
		}
	}
	
	

	
}
