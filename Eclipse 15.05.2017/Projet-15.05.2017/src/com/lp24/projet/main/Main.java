package com.lp24.projet.main;

import java.io.FileNotFoundException;

import javax.swing.JOptionPane;

import com.lp24.projet.controller.Game;
import com.lp24.projet.model.GameState;
import com.lp24.projet.view.Scene;
import com.lp24.projet.view.Window;




public class Main {
	public static Game game;//Will contain our level
	public static String playerName;//Not used for now but can be usefull for a score option
	public static GameState gamestate=GameState.Menu;//Define our position in the programm, see more details in GameState class
	public static String language ="en";//Define language of the program, can be switched in the game to french
	
	public static void choix_nom()
	{
		/*
		 * A function wictch allow to give an username if we need it
		 */
	    String nom = JOptionPane.showInputDialog(null, "Please enter a name", "Lemming", JOptionPane.QUESTION_MESSAGE);
	    		//Open a window wich ask an username
	    
	    if(nom==""||nom==null)//if the user answer nothing
	    {
	    	playerName="Anonyme";
	    }
	    else
	    {
	    	playerName=nom;
	    }
	    
	}
	
	public static void main(String[] args) throws FileNotFoundException {
	
		//choix_nom(); //Here we don't use username so we don't have to use this function
		Scene scene = new Scene();
		Window window = new Window(scene);
		while(true)//Infinite loop
		{
			window.repaint();//Will use paintComponent method of Scene class
		}
		
	}

}
